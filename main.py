from flask import Flask, request, render_template
import secrets
from datetime import datetime, timedelta
from database import cur, con
import uuid
import hashlib
from auth_dec import token_required

app = Flask(__name__)


@app.get('/')
def home_page():
    return render_template('index.html')


@app.get('/tasks')
@token_required
def get_all_tasks(_):
    res = cur.execute('SELECT * FROM tasks')
    tasks = res.fetchall()

    response = []
    for task in tasks:
        response.append({
            'id': task[0],
            'title': task[1],
            'completed': task[2],
            'user_id': task[3]
        })

    return {
        'status': 'OK',
        'message': 'Tasks fetched successfully',
        'data': response
    }


@app.post('/tasks')
@token_required
def create_task(user_id):
    task_id = str(uuid.uuid4())
    task = (task_id, request.json['title'], 0, user_id)
    cur.execute(
        'INSERT INTO tasks (id, title, status, user_id) VALUES (?, ?, ?, ?)', task)

    con.commit()

    return {
        'status': 'OK',
        'message': 'Task has been created successfully!'
    }


@app.post('/login')
def login():
    user = dict(request.json)

    res = cur.execute(
        'SELECT id, password FROM users WHERE username=?', (user['username'], ))
    existing_user = res.fetchone()

    if not existing_user:
        return {
            'status': 'ERROR',
            'message': 'This user does not exist'
        }

    hashed_pass = hashlib.md5(user['password'].encode()).hexdigest()
    if hashed_pass != existing_user[1]:
        return {
            'status': 'ERROR',
            'message': 'Password is incorrect'
        }

    session_id = str(uuid.uuid4())
    access_token = secrets.token_urlsafe(32)

    start_time = datetime.now()
    time_delta = timedelta(hours=5)

    expired_at = start_time + time_delta

    session_data = (
        session_id,
        access_token,
        expired_at.isoformat(),
        existing_user[0]
    )
    cur.execute(
        'INSERT INTO sessions (id, access_token, expires_at, user_id) VALUES (?, ?, ?, ?)', session_data)

    con.commit()

    return {
        'status': 'OK',
        'message': 'Logged in successfully',
        'data': {
            'access_token': access_token
        }
    }


@app.post('/register')
def register():
    user = dict(request.json)

    res = cur.execute(
        'SELECT password FROM users WHERE username=?', (user['username'], ))
    existing_user = res.fetchone()

    if existing_user:
        return {
            'status': 'ERROR',
            'message': 'This user exist'
        }

    user_id = str(uuid.uuid4())
    user_pass = hashlib.md5(user['password'].encode()).hexdigest()

    new_user = (user_id, user['full_name'], user['username'], user_pass)
    cur.execute(
        'INSERT INTO users (id, full_name, username, password) VALUES (?, ?, ?, ?)', new_user)

    con.commit()
    return {
        'status': 'OK',
        'message': 'Successfully registered!'
    }


app.run('0.0.0.0', 8080)
