import sqlite3

con = sqlite3.connect('tasks_app.db', check_same_thread=False)
cur = con.cursor()

# enable foreign keys
cur.execute('PRAGMA foreign_keys = ON;')

# create users table
cur.execute('''
    CREATE TABLE IF NOT EXISTS users (
        'id' TEXT PRIMARY KEY,
        'full_name' TEXT,
        'username' TEXT,
        'password' TEXT
    );
''')

# create sessions table
cur.execute('''
    CREATE TABLE IF NOT EXISTS sessions (
        'id' TEXT PRIMARY KEY,
        'access_token' TEXT,
        'expires_at' TEXT,
        'user_id' TEXT,
        FOREIGN KEY (user_id) REFERENCES users(id)
    );
''')

# create tasks table
cur.execute('''
    CREATE TABLE IF NOT EXISTS tasks (
        'id' TEXT PRIMARY KEY,
        'title' TEXT,
        'status' INTEGER, -- 0 new, 1 in-progress, 2 completed,
        'user_id' TEXT,
        FOREIGN KEY (user_id) REFERENCES users(id)
    );
''')