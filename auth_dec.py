from functools import wraps
from flask import request
from database import cur
from dateutil import parser
from datetime import datetime


def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        if 'authorization' not in request.headers:
            return {
                'status': 'ERROR',
                'message': 'access_token in authorization header is not present'
            }

        access_token = request.headers['authorization']

        res = cur.execute(
            'SELECT user_id, expires_at FROM sessions WHERE access_token=?', (access_token, ))

        session = res.fetchone()
        if not session:
            return {
                'status': 'ERROR',
                'message': 'Such session does not exists'
            }

        user_id = session[0]
        expire_time = parser.parse(session[1])
        current_time = datetime.now()

        if current_time > expire_time:
            return {
                'status': 'ERROR',
                'message': 'This access_token has been expired'
            }

        return f(user_id, *args, **kwargs)
    return decorator
