
-- enable foreign key constraints
PRAGMA foreign_keys = ON;

-- Creating tables 
CREATE TABLE IF NOT EXISTS users (
    'id' TEXT PRIMARY KEY,
    'full_name' TEXT,
    'username' TEXT,
    'password' TEXT
);

CREATE TABLE IF NOT EXISTS sessions (
    'id' TEXT PRIMARY KEY,
    'access_token' TEXT,
    'expires_at' TEXT,
    'user_id' TEXT,
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS tasks (
    'id' TEXT PRIMARY KEY,
    'title' TEXT,
    'status' INTEGER, -- 0 new, 1 in-progress, 2 completed,
    'user_id' TEXT,
    FOREIGN KEY (user_id) REFERENCES users(id)
);